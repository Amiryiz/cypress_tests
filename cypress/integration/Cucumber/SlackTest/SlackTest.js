import {Given, And, When,Then} from "cypress-cucumber-preprocessor/steps"

Given('I visit Slack Site', () => {
    cy.visit('https://slack.com')
})

When('I click on Sign in button', () => {
    cy.get('.u-margin-bottom--flush > a').click()
})

And('I fill the name of workspace with {string} value', (workspaceName) => {
    cy.get('#domain').type(workspaceName)
})

And('I click on Continue button', () => {
    cy.get('.ladda-label').click()
})

Then('A text massage: We couldn’t find your workspace. should appear in the validation errors region', () => {
    cy.contains("We couldn’t find your workspace").should('to.exist')
})

Then('A text massage: Sign in to. should appear in the page', () => {
    cy.contains("Sign in to").should('to.exist')
})

When('I login as user with {string} and {string}', (EMail , password) => {
    cy.get('#email').focus().clear()
    cy.get('#email').focus().type("amiryiz@yahoo.com")//can't remove special characters with parameter -it's a bugggggg !!!!!
    cy.get('#password').clear()
    cy.get('#password').focus().type(password)
    cy.get('#signin_btn').click()
    //cy.visit('https://app.slack.com/client/TMQ8J87B3')
    //Cypress.config('defaultCommandTimeout', 100000)

})

Then('I click on Signin button', () => {
    cy.get('#signin_btn').click()
    cy.get('#team-menu-trigger').should('to.exist')
})

